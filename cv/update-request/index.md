# CV Update request from MSP

Previous CV update was on 15 Nov, 2020. We need to sync CV and https://narkhov.pro with actual state.

## Updates

### Job and Skills

https://narkhov.pro/job.html

#### ✅ Projects at MultiSafePay

* Add **BNPL**;

> [Buy Now, Pay Later (BNPL)](https://www.multisafepay.com/blog/buy-now-pay-later-the-future-of-online-payments) is a type of short-term financing that allows consumers to make purchases and pay for them at a future date, often interest-free. BNPL has been one of the major trends in ecommerce. Added benefits for businesses are the lower cart abandonment rate and higher order value associated with a BNPL offering. These factors and the ever-increasing popularity, have led to Bank of America estimating that total BNPL transaction value will surpass a trillion dollars worldwide by 2025.

* Add **Installments**;

> [Installments](https://money.com/online-shopping-pay-in-installments/) have become a major target for innovation beetwen the providers that allow consumers to pay the order partially. This has allowed these services to become adaptable to many different websites and apps so that the payment process is frictionless and fast no matter where customers choose to shop. MultiSafePay one of the first installments providers in Netherlands and Spain;

* Add [OpenBank](https://help.shopify.com/en/manual/payments/shop-pay-installments) integration.

> [Zinia](https://www.fintechfutures.com/2022/01/santander-enters-bnpl-market-with-launch-of-new-platform-zinia/) BNPL platform: API integration with OpenBank. Zinia offers customers the opportunity to pay in interest-free instalments in a matter of seconds, either online or through physical points of sale.

#### ✅ Projects at SRISA:

* [IEC 61131-3](https://en.wikipedia.org/wiki/IEC_61131-3) development environment implementation and maintenance: [Beremiz](https://beremiz.org/) IDE port for non-linux PLC backends (RTOS, barebones);
* REST RPC protocols and services development for low-resource platforms (PLC, IoT, smart devices);
* System architect: environments with high-level decentralization (PLCs, OPC/IO servers, SCADA stations) and heterogeneous communication protocols;
* API libraries implementation and maintenance for industrial protocols: [ModBUS](https://en.wikipedia.org/wiki/Modbus), [IEC 60870-5](https://en.wikipedia.org/wiki/IEC_60870-5)-101/102/103/104, [EtherCat](https://en.wikipedia.org/wiki/EtherCAT), [OPC UA](https://en.wikipedia.org/wiki/OPC_Unified_Architecture), [NTP](https://en.wikipedia.org/wiki/Network_Time_Protocol), [MQTT](https://en.wikipedia.org/wiki/MQTT), [CAN](https://en.wikipedia.org/wiki/CAN_bus).

#### ✅ Skills

* High loaded backends in Perl;

### Experience

https://narkhov.pro/experience.html

#### ✅ Presentations

* add FOSDEM 2021;
* add German Perl and Raku workshop 2021;
* add The First Raku Conference 2021;
* add FOSDEM 2022.

#### ✅ I’ve advanced experiense in

* update module list at **Raku/Perl6: developing and contributing modules**;
* add details about setup execution and consensus layers for Ethereum nodes (Goerli with Beacon-Geth pair);
* [decentralized authentication](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022#table-of-contents);

#### ✅ Community posts

* add 2021 and 2022 Advent posts.
