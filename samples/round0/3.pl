sub has_unsafe_tags {
    my $string = shift;

    # no data - always safe 
    return unless defined $string;

    my @issues = ();

    if ($string =~ m/<\s*script[^>]*?>/i) {
        push @issues, "script";
    }

    if ($string =~ m/<\s*iframe[^>]*?>/i) {
        push @issues, "iframe";
    }

    if (@issues) {
        Log(3, "unsafe data: " . join(",", @issues));
        return 1;
    }

    # data is safe
    return 0;
}