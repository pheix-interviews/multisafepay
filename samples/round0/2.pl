sub LogFile($$)
{
        my ($filename, $data)=@_;

        my $datetime=TimeString(); $datetime=~s/(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/$1.$2.$3 $4:$5:$6/;
        my $date=$1.'-'.$2.'-'.$3;
        if(!open(LOGFILE, ">>$LogDir/$filename"))
        {
                Log(4, "Cannot open logfile ($filename) for writing: ".$!);
                return 0;
        };
        print LOGFILE "-- Logging started at $datetime --\n";
        print LOGFILE $data."\n";
        print LOGFILE "-- End. --\n\n";
        close(LOGFILE);
        1;
};
