sub getfile($)
{
        my $filename = shift;
        local *F;
        if(open(F, "< $filename"))
        {
                local $/ = undef;  # Read entire file at once
                return <F>; # Return file as one single `line'
        };  # F is automatically closed here
        return undef;
};
