package Shared;

use strict;
use warnings;
use feature qw(say state);

state $use    = 0;
state $create = 0;
state $import = 0;

BEGIN {
    $import++;
}

sub import {
    $use++;
}

sub new {
    $create++;

    bless {};
}

sub info {
    sprintf(
        "%s: used %d times, created %d times",
        __PACKAGE__,
        $use,
        $create
    );
}

1;
