package Foo;

use strict;
use warnings;
use feature qw(say state);

sub new {
    bless {};
}

sub shared {
    Shared->new->info;
}

1;
