#!/usr/bin/env perl

use strict;
use warnings;
use feature qw(say);

use Shared;
use Foo;

say Shared->new->info;
say Foo->new->shared;

__END__;
