package Shared;

use strict;
use warnings;
use feature qw(say state);

state $use;
state $create;

BEGIN {
    $use++;
}

sub new {
    $create++;

    bless {};
}

sub info {
    sprintf("%s: used %d times, created %d times", __PACKAGE__, $use, $create);
}

1;
