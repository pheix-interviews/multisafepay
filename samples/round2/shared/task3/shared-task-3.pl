#!/usr/bin/env perl

use strict;
use warnings;
use feature qw(say);

use Bar;
use Foo;

say Bar->new->get_global;

Foo->new->set_global(0);

say Foo->new->get_global;

Bar->new->set_global(2000);

say Bar->new->get_global;

__END__;
