package Shared;

use strict;
use warnings;

my $global = {
    value => 100
};

sub get_global {
    return $global;
}

1;
