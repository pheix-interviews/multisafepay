package Bar;

use strict;
use warnings;

use Shared;

sub new {
    bless {};
}

sub get_global {
    Shared::get_global->{value};
}

sub set_global {
    my $self = shift;

    local Shared::get_global->{value} = shift;
}

1;
