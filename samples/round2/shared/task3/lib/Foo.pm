package Foo;

use strict;
use warnings;
use feature qw(say state);

use Shared;

sub new {
    bless {};
}

sub get_global {
    Shared::get_global->{value};
}

sub set_global {
    my $self = shift;

    Shared::get_global->{value} = shift;
}

1;
