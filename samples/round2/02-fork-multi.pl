#!/usr/bin/env perl

use strict;
use warnings;
use feature qw(say);

my $kid;
my $max_workers = shift || 1;

for (1..$max_workers) {
    my $pid = fork;
    die sprintf("failed to fork: %s", $!) unless defined $pid;

    next if $pid;

    sleep $_ and say sprintf("child %03d: I am here!", $$) and exit;
}

do {
    say 'Guys, are you there?';
} while ($kid = waitpid -1, 0 and $kid > 0);
