#!/usr/bin/env perl

use strict;
use warnings;
use feature qw(say);

my $pid = fork;

if (!$pid) {
    say 'Who am I?';
    sleep 1 and exit;
}

waitpid $pid, 0;

say 'You are my child!'
