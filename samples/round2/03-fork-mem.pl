#!/usr/bin/env perl

use strict;
use warnings;
use feature qw(say);

#use Devel::Peek;
use Devel::Size qw(total_size);

# $Devel::Peek::pv_limit = 12;

use constant {
    MULT     => 1_000,
    ELEMENTS => 20_000,
    MBYTE    => 1_000_000,
    STR      => q{.} x 32

};

my $child_pid;

if (!defined($child_pid = fork)) {
    # smth wrong with fork
    die sprintf("cannot fork: %d", $!);
}
elsif ($child_pid) {
    # parent proc
    waitpid $child_pid, 0;
}
else {
    # child proc
    my $collection = [(q{}) x ELEMENTS];

    # fill collection
    for (0..(ELEMENTS - 1)) {
        $collection->[$_] = STR x MULT;
    }

    # refill collection
    for (0..(ELEMENTS - 1)) {
        $collection->[$_] = STR;
    }

    say sprintf("collection size %.02f MB", (total_size($collection) / MBYTE));
}
