#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';

use utf8;
use open qw(:std :utf8);

my $input = $ARGV[0];

utf8::decode($input);

if ($input =~ /^[\+\-\.\d]+$/) {
    say "$input is numeric";
}
else {
    say "input <$input> is not numeric";
}

__END__;

echo ௪௰௯௮ | perl -M"open qw(:std :utf8)" -Mre=debug -lpe "/[\d]+/"
