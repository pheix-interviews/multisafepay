#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';

my $hashref = {
    lastname  => 'Doe',
    firstname => 'John',
};

package FooCheck;

sub new {
    return bless {};
}

sub email_exists {
    my ($self, $h) = @_;

    die "no email record for $h->{firstname} $h->{lastname}"
        unless not exists $h->{email};

    return 1;
}

package main;

if (FooCheck->new->email_exists($hashref)) {
    say "user has email record";
}

__END__;
