#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';

package POSIX;

sub setsid {
   my $self = shift;
   return 20210424;
}

package main;

use POSIX;
use Data::Dumper;

sub show_sessid {
    my $sess_id = POSIX::setsid();
    say Dumper($sess_id);
}

show_sessid();

__END__;
