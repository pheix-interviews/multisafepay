#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';

use constant ITERS => 20;

foreach my $iter (0..ITERS) {
    if ($iter == ITERS/2) {
        die "proudly died at $iter iteration";
    }
}

say "debugger rules!"

__END__;
