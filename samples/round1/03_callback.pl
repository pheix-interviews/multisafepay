#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';

package Callback;

sub new {
    return bless {}, __PACKAGE__;
}

sub process {
    my ($self, $path) = @_;

    if (-e -d $path) {
        ; # do smth
    }

    return;
}

package main;

my $callback_succeed = eval { Callback->new->process('/usr/bin'); };

if ($callback_succeed) {
    say "Callback ok";
}
else {
    say "Callback failed"
}

__END__;
