#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';

package Solver;

use Scalar::Util qw(looks_like_number);

sub new {
    my ($class, %args) = @_;

    my $self  = {
        x   => $args{x},
        y   => $args{y},
        sum => $class->_x_plus_y($args{x}, $args{y}),
    };

    bless $self, $class;
    return $self;
}

sub _x_plus_y {
    my ($self, $x, $y) = @_;
    return ($x + $y) if (looks_like_number($x) and looks_like_number($y));
}

package main;

use Test::More;

is(Solver->new(x => 2, y => 2)->{sum}, 4, '2+2=4');
is(Solver->_x_plus_y(10, 15), 25, '_x_plus_y method: 10+15=25');
is(Solver->new(x => 0xf, y => 0xd)->{sum}, 0x1c, 'hex ok');
ok(!Solver->new(x => 'y', y => 'x')->{sum}, 'chars not ok');

done_testing();
