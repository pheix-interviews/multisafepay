#!/usr/bin/env perl

use strict;
use warnings;
use feature 'say';

my $ok = eval {
    my $x   = 0;
    my $div = 1 / $x;

    1;
} or do {
    my $error_message = $@;
    say sprintf("error: %s", $error_message);

    2;
};

if ($ok) {
    say "success";
}
else {
    say "failure";
}
