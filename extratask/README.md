# TASK: create Perl module for remote REST HTTP service

There's a simple remote REST HTTP service, It accepts two types of requests:

* retrieve data request;
* commit data request.

Implement Perl module with simple API to cover REST HTTP service functionality.

## Service URLS

* retrieve requests: http://78.47.192.226:8085/request-r.html
* commit requests: http://78.47.192.226:8085/request-w.html

## Requests and responses JSON schemes

Schemes are available here: https://gitlab.com/pheix-scada/io-sw/-/tree/devel/perl/json_schemes

## Data model

Simple remote REST HTTP service operates with only the next data tags (see JSON schemes for details): `parameter_1`, `parameter_2` and `parameter_3`.

### Retrieve data example

```bash
#!/bin/bash

curl --header "Content-Type: application/json" --request POST --data '{"controller_id":40621, "r_request":{"tags": ["parameter_1", "parameter_2", "parameter_3"]}}' http://78.47.192.226:8085/request-r.html
```

### Commit data example

```bash
#!/bin/bash

curl --header "Content-Type: application/json" --request POST --data '{"controller_id":40621, "w_request":{"tags": [{"tag":"parameter_1", "value":777}, {"tag":"parameter_2", "value":555}, {"tag":"parameter_3", "value":333}]}}' http://78.47.192.226:8085/request-w.html
```

## Push your job

Please store your module at `extratask/module` after the task is completed.
